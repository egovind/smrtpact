//
//  AppDelegate.h
//  XTTemp
//
//  Created by KG on 21/10/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDLog.h"
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (atomic) bool hasAuthenticated;
@property (atomic) bool debugMode;

@end

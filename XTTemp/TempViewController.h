//
//  FirstViewController.h
//  XTTemp
//
//  Created by KG on 21/10/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XTProtocol.h"
#import "DDLog.h"
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"
#import "MediaPlayer/MediaPlayer.h"
#import "AudioToolbox/AudioToolbox.h"
#import "AVFoundation/AVFoundation.h"
#import "AppDelegate.h"

#define C_TEMP_READ_INTERVAL_S  ((double)500/1000)
#define C_CONN_STATUS_INTERVAL_S  ((double)1000/1000)
#define C_NO_OF_ROWS_PER_SECTION 1u
#define LOGGING 1u
#define C_MODE_DEMO     0
#define C_MODE_DEBUG    1

@interface TempViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, mfiDelegate, XTProtocolDelegate>{
 // Class private members go here
    // EA Protocol
    //XTProtocol* XTPack;
    bool firstConnect;
    NSTimer *_samplingTimer;
    bool measurementInProgress; //Flag to lock measurement
#if (LOGGING == 1)
    DDFileLogger *fileLogger;
#endif
}

@property (nonatomic, strong) IBOutlet UITableView *tempTableView;
@property (nonatomic, strong) IBOutlet UILabel *labelAccStatus;
@property (nonatomic, strong) IBOutlet UILabel *labelMode;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer; // Need this to prevent ARC from releasing audion player instance and not playing BEEP
@property (nonatomic, strong) IBOutlet UILabel *objectTemperature;
@property (atomic) int displayMode; // Demo or Debug
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
#if (LOGGING == 1)
@property (nonatomic, readonly) DDFileLogger *fileLogger;
#endif

// Labels for debug screen


@end



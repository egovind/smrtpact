//
//  protocolDemo1.h
//  MCHP MFI
//
//  Created by Joseph Julicher on 6/10/10.
//  Copyright 2010 Microchip Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "mfi.h"
#import "DDLog.h"

@protocol XTProtocolDelegate
- (void) newMeasurementComplete:(float)temp;
- (void) shutterPressed;
@end

// Constants for battery pack
#define C_BAT_PACK_VBAT_LOW     1u
#define C_BAT_PACK_FLT          2u

#define C_INDEX_AMBIENT_TEMP    0u
#define C_INDEX_OBJECT_TEMP     1u
#define C_INDEX_RAW_IR1         2u
#define C_INDEX_RAW_IR2         3u

#define TS_EEPROM_TO_MAX                    (uint8_t) ((0x00) | (0x20))
#define TS_EEPROM_TO_MIN                    (uint8_t) ((0x01) | (0x20))
#define TS_EEPROM_PWMCTL                    (uint8_t) ((0x02) | (0x20))
#define TS_EEPROM_TA_RANGE                  (uint8_t) ((0x03) | (0x20))
#define TS_EEPROM_EMMISIVITY                (uint8_t) ((0x04) | (0x20))
#define TS_EEPROM_CONFIG1                   (uint8_t) ((0x05) | (0x20))
#define TS_EEPROM_SMB_ADDRESS               (uint8_t) ((0x0E) | (0x20))
#define TS_EEPROM_ID1                (uint8_t) ((0x1C) | (0x20)) //0x3C
#define TS_EEPROM_ID2                (uint8_t) ((0x1D) | (0x20)) //0x3D
#define TS_EEPROM_ID3                (uint8_t) ((0x1E) | (0x20)) //0x3E
#define TS_EEPROM_ID4                (uint8_t) ((0x1F) | (0x20)) //0x3F

#define TS_CONFIG_MASK_LOW_IIR                0x07 // IIR
#define TS_CONFIG_MASK_HIGH_FIR               0x07 // FIR

#define C_KELVIN_TO_CELSIUS     (double)(273.15)

#define C_DEFAULT_BODY_MODE_OFFSET  (float)3.3
//NSString *const XTParametersChangedNotification;

@interface XTProtocol : mfi {

	NSThread *updateThread;
    float temperature;
    int adc;
    uint8_t powerBankStatus;
    BOOL sessionActive;
    float emmissivity;
    int samples;

}


+ (XTProtocol*)instance;


@property (nonatomic, weak) id <XTProtocolDelegate> delegateXT;

@property (nonatomic,strong) NSMutableArray* TemperatureData; // Array that stores the filtered temperature for display
@property (nonatomic,strong) NSNumber* avgPoints; // Number of samples to average
@property (atomic) int settlingTime;
@property (atomic) int samplingInterval;
@property  (atomic)    uint8_t configIIR;
@property (atomic)  uint8_t configFIR;
@property (nonatomic,strong) NSMutableArray* AmbTempBuffer;
@property  (nonatomic,strong) NSMutableArray*  ObjTempBuffer;
@property (atomic) float offsetBodyMode;
@property (atomic) bool bodyMode;

- (void) pollADCValue;
- (int) getADCValue;
- (void) pollBatLow;
- (void) readBatLow;
- (uint8_t) getPowerBankStatus;
- (void) newControlChargerCmd:(BOOL)OnorOff;
- (void) readTemperature;
- (void) updateAccStatus:(bool) status;
- (bool) getSessionStatus;
- (NSNumber*) getEmissivity;
- (void) writeMLXRegister:(uint8_t)reg withValue:(uint16_t)value;
- (void) readMLXRegister:(uint8_t)reg;
- (void) startNewMeasurement;
- (void) resetXTState;
- (void) writeConfigRegister:(uint8_t)fir withParam:(uint8_t)iir;

@end

//
//
//
// Note on handling of sessions + connect/disconnect:
// 1. Keep the Protocol object as a Singleton.
// 2. It's created one time on app launch in the ViewDidLoad method, Hook up the connect/disconn notifiers here.
// 3. When the app goes into background or device is disconnected, close the session and streams; BUT retain the protocol object + notifications
// 4. When app is relaunched, depending on the whether the phone went into full standby, the accessory will take little or more time to opean the EA session
// 5. So we need to be able to re-open the session either when the app comes into FG OR when the accessory is re-connected. applicationWillEnterForeground may be called before accessory is able to open the EA session.
// So we use the connected notification to re-open the session + streams if they are closed.
// 6. Similarly session may be closed when accessory disconnects OR app goes into BG.

#import "mfi.h"

@implementation mfi
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

// OVERLOAD ME!!
- (int) readData:(NSData *) data
{
	return (int)[data length];
}

- (id) initWithProtocol:(NSString *)protocol
{
	theProtocol = protocol;
	// see if an accessory is already attached
	eas = [self openSessionForProtocol:theProtocol];
    if(eas == nil)
    {
        // we did not find an appropriate accessory
    }
	// install notification events.
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(accessoryDidConnect:)
												 name:EAAccessoryDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(accessoryDidDisconnect:)
                                                 name:EAAccessoryDidDisconnectNotification object:nil];
    
    [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    return self;
}

- (bool) isConnected
{
    if([[eas accessory] isConnected] == true){
        //  NSLog(@"ret acc Conn");
        return ([[eas accessory] isConnected]);
    }
    else{
        //  NSLog(@"ret acc disConn");
        return (XTFlashIsConnected);
    }
}

- (NSString *) name { return (eas)?[[eas accessory] name]:@"-"; }
- (NSString *) manufacturer { return (eas)?[[eas accessory] manufacturer]:@"-";}
- (NSString *) modelNumber {return (eas)?[[eas accessory] modelNumber]:@"-";}
- (NSString *) serialNumber {return (eas)?[[eas accessory] serialNumber]:@"-";}
- (NSString *) firmwareRevision {return (eas)?[[eas accessory] firmwareRevision]:@"-";}
- (NSString *) hardwareRevision {return (eas)?[[eas accessory] hardwareRevision]:@"-";}

/***********************************************************************/
#pragma mark External Accessory Basic Identification
/***********************************************************************/
// Note that you can only open a session if the accessory is already connected!
- (EASession *)openSessionForProtocol:(NSString *)protocolString
{
    /*----- Code for Logging ---- */
    // NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    // NSString *logPath = [cachesDirectory stringByAppendingPathComponent:@"application.log"];
    // freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
    /*----- Code for Logging ---- */
    DDLogVerbose(@"getting a list of accessories");
    NSArray *accessories = [[EAAccessoryManager sharedAccessoryManager]
                            connectedAccessories];
	
    EAAccessory *accessory = nil;
    EASession *session = nil;
    
	DDLogVerbose(@"Found %lu accessories",(unsigned long)[accessories count]);
    DDLogVerbose(@"looking for %@",protocolString);
    
	for (EAAccessory *obj in accessories)
    {
		NSArray *sa = [obj protocolStrings];
		DDLogVerbose(@"a string dump of %lu strings",(unsigned long)[sa count]);
		
		for(NSString *s in sa)
		{
			NSLog(@"%@",s);
		}
		
        if ([[obj protocolStrings] containsObject:protocolString])
        {
            accessory = obj;
            break;
        }
    }
    
    NSLog(@"Scanned the list of accessories");
    if (accessory)
    {
        session = [[EASession alloc] initWithAccessory:accessory
                                           forProtocol:protocolString];
        if (session)
        {
            DDLogVerbose(@"opening the streams for this accessory");
            [[session inputStream] setDelegate:self];
            [[session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                             forMode:NSDefaultRunLoopMode];
            [[session inputStream] open];
            [[session outputStream] setDelegate:self];
            [[session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                              forMode:NSDefaultRunLoopMode];
            [[session outputStream] open];
            streamReady = true;
            receivedAccPkt = true;
        }
    }
    return session;
}

- (void)dealloc{
    if(eas != nil){
     [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidConnectNotification object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidDisconnectNotification object:nil];
    }
}

#pragma mark Stream Processing
// Handle communications from the streams.
- (void)stream:(NSStream*)stream handleEvent:(NSStreamEvent)streamEvent
{
    uint8_t buf[30];
    unsigned int len = 0;
	int count = 0;
    
    switch (streamEvent)
    {
        case NSStreamEventHasBytesAvailable:
        {
            DDLogVerbose(@"NSStreamEventHasBytesAvailable");
            // Process the incoming stream data.
            // get the new bytes from the stream
            while((len = (int) [(NSInputStream *)stream read:buf maxLength:sizeof(buf)]))
            {
				if(rxData == nil) rxData = [NSMutableData data];
				[rxData appendBytes:buf length:len];
				count += len;
                DDLogVerbose(@"rxData: %@", rxData);
                DDLogVerbose(@"Frame lentgh: %lu \r\n",(unsigned long)rxData.length);
			}
			
			// loop here to unload the receive queue
			// find a preamble
			const char preambleBytes[] = {0x5A,0xA5};
			NSRange r = {0,sizeof(preambleBytes)};
			NSData	*pd = [NSData dataWithBytes:preambleBytes length:2];
            
			r.length = 2;
			while ((r.length + r.location) < [rxData length])
			{
				if([[rxData subdataWithRange:r] isEqualToData:pd])
				{
					if( (r.location + r.length) > [rxData length])
						break;
					r.location += 2;
					r.length = [rxData length] - r.location;
					len = [self readData:[rxData subdataWithRange:r]];
					r.location += len;
				}
				else
				{
					r.location ++;
				}
				
				r.length = 2;
			}
            
			if(r.location >= [rxData length])
			{
				rxData = nil;
			}
			else
			{
				// reset the receiver with the bytes that were not consumed.
				[rxData setData:[rxData subdataWithRange:r]];
			}
        }
			
            break;
            
        case NSStreamEventHasSpaceAvailable:
        {
            DDLogVerbose(@"NSStreamEventHasSpaceAvailable");
            // Send the next queued command.
			@synchronized(self)
			{
				[self txBytes];
                
			}
        }
            break;
        case NSStreamEventErrorOccurred:
        {
            DDLogVerbose(@"Stream error Occured");
            break;
        }
            
        case NSStreamEventEndEncountered:
        {
            DDLogVerbose(@"Stream End Encountered");
            txData = nil;
            break;
        }
        default:
        {
            break;
        }
    }
}

- (void) closeEASession
{
    if(eas != nil){
        DDLogVerbose(@"Closing Accessory Stream");
        [[eas inputStream] close];
        [[eas outputStream] close];
        [[eas outputStream] removeFromRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        [[eas inputStream] removeFromRunLoop:[NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        eas = nil;
        txData = nil;
        rxData = nil;
        streamReady = false;
        // Don't remove the notifications !! see note above.
        // [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidConnectNotification object:nil];
        // [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidDisconnectNotification object:nil];
    }
}

- (void) openEASession
{
    if(eas == nil)
        eas = [self openSessionForProtocol:theProtocol]; // eas will be non-nil only if we were able to open a session
}

#pragma mark accessory notifications

/* this is the notification function if we register for the did disconnect notification event */
- (void)accessoryDidDisconnect:(NSNotification *)notification
{
    DDLogVerbose(@"Accessory Disconnected");
    [self closeEASession];
    XTFlashIsConnected = false;
    id delegate = [self delegate];
    if ([delegate respondsToSelector:@selector(accessoryConnectionStatusChanged:)]) {
        [delegate accessoryConnectionStatusChanged:FALSE];
    }
    
}

- (void)accessoryDidConnect:(NSNotification *)notification
{
    if(XTFlashIsConnected){
        DDLogVerbose(@"Accessory Already Connected");
        return;
    }
    
    DDLogVerbose(@"Accessory Connected");
    // YEA we got an accessory
    // nothing to do yet
    XTFlashIsConnected = true;
    // This method is recieved in response to the EAAccessoryDidConnectNotification event
    [self openEASession];// if session is closed, open up again
    if(eas != nil)
    {
        // YEA we got an accessory
        // nothing to do yet
        XTFlashIsConnected = true;
        id delegate = [self delegate];
        if ([delegate respondsToSelector:@selector(accessoryConnectionStatusChanged:)]) {
            [delegate accessoryConnectionStatusChanged:TRUE];
        }
    }
    txData = nil; // reset the tx stream, if this is not done, then there is a possibility that the app is in the middle of a tx when it goes into standby. When the phone comes out of stdby, txData != 0, but the data is lost and no more bytes can be sent over
    
}


#pragma mark Stream Handling TX and RX functions

-(void) queueTxBytes:(NSData *)buf
{
    if(streamReady == FALSE){
        DDLogVerbose(@"StreamDead");
        return;
    }
    
    
    unsigned char p[] = {0x5A,0xA5};
	
	if([self isConnected])
	{
		if(txData!=nil)
		{
			[txData appendData:[NSData dataWithBytes:p length:sizeof(p)]]; //Govind - disable preamble
			[txData appendData:buf];
            // Force send here.gvm
            // Govind - If you wait till the run-loop sends the data, you wont get any messages during picture capture
            if([[eas outputStream] hasSpaceAvailable])
			{
				@synchronized(self)
				{
					[self txBytes]; // jumpstart the transmitter
				}
			}
		}
		else
		{
			txData = [NSMutableData dataWithBytes:p length:sizeof(p)]; //Govind - disable preamble
			[txData appendData:buf];
            //   [txData appendData:[NSData dataWithBytes:(__bridge const void *)(buf) length:[buf length]]];
			if([[eas outputStream] hasSpaceAvailable])
			{
				@synchronized(self)
				{
					[self txBytes]; // jumpstart the transmitter
				}
			}
		}
	}
}

-(void) txBytes
{
    int len;
    
    if(txData!=nil)
    {
        if([txData length])
        {
            len = (int)[[eas outputStream] write:[txData bytes] maxLength:[txData length]];
            DDLogVerbose(@"Tx:%@", txData);
            if (len < [txData length])
			{
				NSRange range;
				range.location = len;
				range.length = [txData length] - len;
                // some data remaining
				[txData setData:[txData subdataWithRange:range]];
            }
            else
			{ // no data remaining
				txData = nil;
            }
        }
    }
	else{
        DDLogVerbose(@"Tx Pointer = NIL");
    }
}

typedef enum{ find_preamble,get_com_cmd,get_mddfs_cmd,get_mddfs_data } mddfs_rx_states_t;
-(void) rxBytes:(const void*)buf length:(int)len
{
    static mddfs_rx_states_t receive_state = find_preamble;
	
    if (receive_state != find_preamble)
    {
        [rxData appendBytes:buf length:len];
    }
    else // look for the preamble
    {
        for(int x=1;x<len;x++)
        {
            if(((uint8_t *)buf)[x-1] == 0x5A && ((uint8_t *)buf)[x] == 0xA5)
            {
                receive_state = get_com_cmd;
                rxData = [NSMutableData data];
                [rxData appendBytes:&((uint8_t *)buf)[x+1] length:len-x-1];
                break;
            }
        }
    }
}

@end

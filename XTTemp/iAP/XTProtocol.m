//
//  protocolDemo1.m
//  MCHP MFI
//
//  Created by Joseph Julicher on 6/10/10.
//  Copyright 2010 Microchip Technology. All rights reserved.
//

#import "XTProtocol.h"

static XTProtocol* inst = nil;
@interface XTProtocol ()



@end

@implementation XTProtocol
@synthesize TemperatureData;
@synthesize avgPoints;
@synthesize AmbTempBuffer;
@synthesize ObjTempBuffer;

static const int ddLogLevel = LOG_LEVEL_VERBOSE ;//LOG_LEVEL_VERBOSE; LOG_LEVEL_ERROR

static UInt32 TAmbBuffer[50];
static UInt32 TObjBuffer[50];

+ (XTProtocol*)instance {
    if (!inst) inst = [[XTProtocol alloc] init];
    return inst;
}

- (void) updateAccStatus:(bool) status
{
    if(!status)
    sessionActive = false;
}

- (bool) getSessionStatus{
    return(sessionActive);
}

- (double) convertRawToCelsius:(int) lsb
{
    return ((double)lsb * 0.02 - C_KELVIN_TO_CELSIUS);
}

- (void) readTemperature
// XT will reply with all the temp values
{
   // if(!sessionActive)
   //     return;
    
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 16;
	buffer[1] = 0;
	buffer[2] = 0;
    NSLog(@"Read Temperature");
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
}

- (void) startNewMeasurement
{
    UInt8 k = 0;
    while(k < (avgPoints.intValue - 1 ))
    {
        TAmbBuffer[k] = 0;
        TObjBuffer[k] = 0;
        k++;
    }
    samples = 0;
    [[self AmbTempBuffer] removeAllObjects];
    [[self ObjTempBuffer] removeAllObjects];
    
}
- (void) MovingAverge:(uint16_t)Tamb withObjT:(uint16_t)Tobj
{

    UInt8 k = 0;
    while(k < (avgPoints.intValue - 1 ))
    {
        TAmbBuffer[k] = TAmbBuffer[k+1];
        TObjBuffer[k] = TObjBuffer[k+1];
        k++;
    }
    TAmbBuffer[avgPoints.intValue - 1] = 0;
    TObjBuffer[avgPoints.intValue - 1] = 0;
    
    k = 0;
    while(k < avgPoints.intValue)
    {
        TAmbBuffer[k] = TAmbBuffer[k] + Tamb;
        TObjBuffer[k] = TObjBuffer[k] + Tobj;
        k++;
    }
    

    NSNumber *temp2 = [NSNumber alloc];

    // In body mode use adjust all temperature with a fixed offset
    if(self.bodyMode == true){
        temp2 = [temp2 initWithDouble:(double)TAmbBuffer[0]/avgPoints.intValue * (double) 0.02 - C_KELVIN_TO_CELSIUS + self.offsetBodyMode];
    }
    else{
        
        temp2 = [temp2 initWithDouble:(double)TAmbBuffer[0]/avgPoints.intValue * (double) 0.02 - C_KELVIN_TO_CELSIUS];
    }
    
    [TemperatureData insertObject:temp2 atIndex:C_INDEX_AMBIENT_TEMP];
    
    NSNumber *temp = [NSNumber alloc];
    
    if(self.bodyMode == true){
    temp = [temp initWithDouble:(double)TObjBuffer[0]/avgPoints.intValue * (double) 0.02 - C_KELVIN_TO_CELSIUS + self.offsetBodyMode];
    }
    else{
    temp = [temp initWithDouble:(double)TObjBuffer[0]/avgPoints.intValue * (double) 0.02 - C_KELVIN_TO_CELSIUS];
    }
    [TemperatureData insertObject:temp atIndex:C_INDEX_OBJECT_TEMP];
    
    DDLogVerbose(@"Temp2 : %d, %d, %f, %f \r\n",Tamb,Tobj,[TemperatureData[C_INDEX_AMBIENT_TEMP]floatValue] , [TemperatureData[C_INDEX_OBJECT_TEMP]floatValue]);
}


- (void) newControlChargerCmd:(BOOL)OnorOff
{
	uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 9;
	buffer[1] = OnorOff;
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
    
}

- (void) resetXTState
{
    
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 20;
	buffer[1] = 0;
	buffer[2] = 0;
    NSLog(@"Reset State");
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
    
}

// When you write a register, the firmware will reply with a read so that you can check if the write was OK
- (void) writeMLXRegister:(uint8_t)reg withValue:(uint16_t)value
{
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 18;
	buffer[1] = reg;
	buffer[2] = (value >> 8) &0x00FF; //MSB
	buffer[3] = value & 0x00FF; //LSB
    NSLog(@"Write Register");
    @synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
}

- (void) readMLXRegister:(uint8_t)reg
{
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 19;
	buffer[1] = reg;
	buffer[2] = 0;
	buffer[3] = 0;
    NSLog(@"Read Register");
    @synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
}

- (void) writeConfigRegister:(uint8_t)fir withParam:(uint8_t)iir
{
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 21;
	buffer[1] = iir & TS_CONFIG_MASK_LOW_IIR; //LSB
	buffer[2] = fir & TS_CONFIG_MASK_HIGH_FIR; //MSB
	buffer[3] = 0;
    NSLog(@"Write Config Register");
    @synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
}


- (void) pollADCValue
{
    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 13;
	buffer[1] = 0;
	buffer[2] = 0;
    NSLog(@"ADC Status Polled");
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
    
}

- (void) pollBatLow
{

    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 14;
	buffer[1] = 0;
	buffer[2] = 0;
    NSLog(@"Power Status Polled");
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
    
}

- (void) readBatLow
{

    uint8_t buffer[6];
	bzero(buffer, sizeof(buffer));
    
	buffer[0] = 15;
	buffer[1] = 0;
	buffer[2] = 0;
    NSLog(@"Power Status Read");
	@synchronized (self)
	{
		[self queueTxBytes:[NSData dataWithBytes:buffer length:6]];
	}
    
}



- (int) readData:(NSData *) data
{
	int ret;
	ret = 0;
	if([data length] >= 6)
	{
		NSRange r;
		uint8_t buf[10];
		r.location = 0;
		r.length = 6;
		ret = r.length;
		[data getBytes:buf length:6]; // Extract the complete Packet
      //  DDLogVerbose(@"rxData2: %@", data);

		// process data received from the accessory
		switch(buf[0])
		{
			case 16: // Temperature data
				@synchronized (self)
				{
                    // FIXME: Test for error - MSB set means error value
                    [self MovingAverge:(buf[1] + buf[2] * 256) withObjT:(buf[3] + buf[4] * 256)];
#if 0
                    int t;
					t = buf[1] + buf[2] * 256;
                    NSNumber *temp = [[NSNumber alloc]initWithDouble:(double)t * (double) 0.02 - C_KELVIN_TO_CELSIUS];
                    [TemperatureData insertObject:temp atIndex:C_INDEX_AMBIENT_TEMP];
                    
                    t = buf[3] + buf[4] * 256;
                    temp = [[NSNumber alloc]initWithDouble:(double)t * (double) 0.02 - C_KELVIN_TO_CELSIUS];
                    [TemperatureData insertObject:temp atIndex:C_INDEX_OBJECT_TEMP];
                    
                    t = buf[5] + buf[6] * 256;
                    temp = [[NSNumber alloc]initWithInt:t];
                    [TemperatureData insertObject:temp atIndex:C_INDEX_RAW_IR1];
                    
                    t = buf[7] + buf[8] * 256;
                    temp = [[NSNumber alloc]initWithInt:t];
                    [TemperatureData insertObject:temp atIndex:C_INDEX_RAW_IR2];
#endif
                   // DDLogVerbose(@"Temp : %d, %d \r\n",buf[1] + buf[2] * 256,buf[3] + buf[4] * 256);
                    DDLogVerbose(@"T_ambient:%3.3f, T_Object:%3.3f",[self convertRawToCelsius:buf[1] + buf[2] * 256], [self convertRawToCelsius:buf[3] + buf[4] * 256]);
                    [[self AmbTempBuffer ]addObject:[[NSNumber alloc]initWithDouble:[self convertRawToCelsius:buf[1] + buf[2] * 256]]];
                    [[self ObjTempBuffer ]addObject:[[NSNumber alloc]initWithDouble:[self convertRawToCelsius:buf[3] + buf[4] * 256]]];
                                                                
                    samples++;
                    if(samples >= avgPoints.intValue )
                    {
                        samples = 0;
                        // Notify the delegate that a measurement is complete
                        id delegate = [self delegateXT];
                        if ([delegate respondsToSelector:@selector(newMeasurementComplete:)]) {
                            [delegate newMeasurementComplete:[TemperatureData[C_INDEX_OBJECT_TEMP]floatValue]];
                        }
                    }
                    
				}
				break;
			case 19: // ReturnAccessorySwitches
				@synchronized (self)
				{
                    if( buf[1] == TS_EEPROM_EMMISIVITY)
                    {
                        emmissivity = (float)(buf[2] + buf[3] * 256)/65535;
                        DDLogVerbose(@"Emissivity : %d, %d,%d \r\n",buf[1],buf[2],buf[3]);
                    }
                    if( buf[1] == TS_EEPROM_CONFIG1)
                    {
                        self.configIIR = buf[2] & TS_CONFIG_MASK_LOW_IIR;
                        DDLogVerbose(@"IIR : %d \r\n",self.configIIR);
                        self.configFIR = buf[3] & TS_CONFIG_MASK_LOW_IIR;
                        DDLogVerbose(@"IIR : %d \r\n",self.configFIR);
                    }
                    
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"XTParametersChangedNotification"
                     object:self];
				}

				break;
			case 6: // Trigger button state and Flash status from Acc
				{
					@synchronized (self)
					{
                        if( buf[1] == 1)
                        {
                            id delegate = [self delegateXT];
                            if ([delegate respondsToSelector:@selector(shutterPressed)]) {
                                [delegate shutterPressed];
                            }
                        }
					}
				}
				break;
			case 13: // ADC
				{
					int p;
					p = buf[1] * 256 + buf[2];
					if(p < 0) p = 0;
					@synchronized (self)
					{
						adc = p;
                        DDLogVerbose(@"ADC value: %d \r\n",adc);
					}
				}
				break;
            case 15: // Power bank status
                @synchronized (self)
                {
                    DDLogVerbose(@"Power bank status : %d, %d,%d \r\n",buf[1],buf[2],buf[3]);
                    powerBankStatus = buf[1] | (buf[2] << 1) | (buf[2] << 2); // ignore other bytes for now
                }
				break;
			default: // unknown command
				DDLogVerbose(@"%@ : Unknown Command %d",theProtocol,buf[0]);
				break;
		}
	}
	return ret;
}

- (int) getADCValue
{
    return(adc);
}

- (uint8_t) getPowerBankStatus;
{
    return(powerBankStatus);
}

- (NSNumber*) getEmissivity
{
    return([NSNumber numberWithFloat:emmissivity]);
}

- (id) init
{
	self = [super initWithProtocol:@"com.xenon-technologies.basic.temp"];
    TemperatureData = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],nil];
    AmbTempBuffer = [[NSMutableArray alloc]init];
    ObjTempBuffer = [[NSMutableArray alloc]init];
    
    for(int i=0; i<10; i++){
        [AmbTempBuffer addObject:[NSNumber numberWithFloat:0.0f]];
        [ObjTempBuffer addObject:[NSNumber numberWithFloat:0.0f]];
    }
    inst = self;
    [self setAvgPoints:[NSNumber numberWithInt:1]]; // By default average one sample only
    self.bodyMode = 0; //Object mode by default
    self.offsetBodyMode = C_DEFAULT_BODY_MODE_OFFSET; //
	return self;
	
}

- (void) enterBackground
{
    // Close streams and de-register from notifications but retain the object
    [self closeEASession];
}

- (void) enterForeground
{
    [self openEASession];
}


@end

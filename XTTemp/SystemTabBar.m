//
//  SystemTabBar.m
//  SmartpacT
//
//  Created by KG on 5/12/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import "SystemTabBar.h"

@interface SystemTabBar ()

@end


@implementation SystemTabBar



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    return;
}
- (BOOL)tabBarController:(UITabBarController *)aTabBar shouldSelectViewController:(UIViewController *)viewController

{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // if (![self hasValidLogin] && (viewController != [aTabBar.viewControllers objectAtIndex:0]) )
    
    {
        
        // Disable all but the first tab.
        
        //   return NO;
        
    }
    if(appDelegate.hasAuthenticated  == TRUE)
        return YES;
    
    if( viewController == [aTabBar.viewControllers objectAtIndex:2] ){
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please enter password for settings"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Continue", nil];
    [message setAlertViewStyle:UIAlertViewStyleSecureTextInput];
    [message setTag:1];
    [message show];
    
    return NO;
    }
    else{
        return YES;
    }
}

- (IBAction)showMessage:(id)sender {

}
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    return YES;
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= 10 )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if( [alertView tag] == 1)
    {
        NSString *inputText = [[alertView textFieldAtIndex:0] text];
        if([inputText isEqualToString:@"louis"]){
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.hasAuthenticated = TRUE;
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Password OK!"
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:nil
                                                otherButtonTitles:@"Continue", nil];
        //[message setAlertViewStyle:UIAlertViewStyleSecureTextInput];
        [message setTag:2];
        [message show];
        }
        else{
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Password Wrong!"
                                                              message:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                                    otherButtonTitles:@"Continue", nil];
            //[message setAlertViewStyle:UIAlertViewStyleSecureTextInput];
            [message setTag:2];
            [message show];
            
        }
    }

}

@end

//
//  SecondViewController.m
//  XTTemp
//
//  Created by KG on 21/10/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@synthesize avgPointsText;
@synthesize emissivityText;
@synthesize samplingIntervalText;
@synthesize settlingTimeText;
@synthesize firText;
@synthesize iirText;
@synthesize offsetText;

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Add this instance of TestClass as an observer of the TestNotification.
    // We tell the notification center to inform us of "TestNotification"
    // notifications using the receiveTestNotification: selector. By
    // specifying object:nil, we tell the notification center that we are not
    // interested in who posted the notification. If you provided an actual
    // object rather than nil, the notification center will only notify you
    // when the notification was posted by that particular object.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(accessoryParametersChanged:)
                                                 name:@"XTParametersChangedNotification"
                                               object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.settlingTimeText.text = [NSString stringWithFormat:@"%i", [[XTProtocol instance]settlingTime]] ;
    self.samplingIntervalText.text = [NSString stringWithFormat:@"%i", [[XTProtocol instance]samplingInterval]] ;
    self.avgPointsText.text = [[[XTProtocol instance]avgPoints]stringValue];
    // read parameters
    if([[XTProtocol instance]isConnected]){
        [[XTProtocol instance]readMLXRegister:TS_EEPROM_CONFIG1];
        [NSTimer scheduledTimerWithTimeInterval:((float)1.0)  target:self selector:@selector(readEmissivityCallback) userInfo:nil repeats:NO];
    }
    
}

- (void) readEmissivityCallback
{
    if([[XTProtocol instance]isConnected])
        [[XTProtocol instance]readMLXRegister:TS_EEPROM_EMMISIVITY];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)kEEditingStarted:(id)sender
{
    DDLogVerbose(@"kEEditingStarted");
    
}
-(IBAction)kEEditingFinished:(id)sender
{
    DDLogVerbose(@"kEEditingOver");
    
}


-(IBAction)enterDebugMode:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.debugMode = TRUE;
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Entered Debug Mode"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Continue", nil];
    [message show];
}
-(IBAction)enterDemoMode:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.debugMode = FALSE;
    appDelegate.hasAuthenticated = FALSE;
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Entered Demo Mode"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Continue", nil];
    [message show];
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    if( avgPointsText == textField){
        DDLogVerbose(@"You entered %@",self.avgPointsText.text);
        NSNumber* value = [f numberFromString:self.avgPointsText.text];
        if( (value.intValue < 1) || (value.intValue > 50) )
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Average is between 1 and 50 samples, please try again." delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        else{
            [[XTProtocol instance]setAvgPoints:value];
        }
        
        [self.avgPointsText resignFirstResponder];
    }
    else if(emissivityText == textField)
    {
        DDLogVerbose(@"You entered %@",self.emissivityText.text);
        NSNumber* value = [f numberFromString:self.emissivityText.text];
        if( (value.floatValue > 1.0) || (value.floatValue < 0.0) )
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Emissivity is between 0 and 1, please try again." delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        else{
            // Valid Ke, update it
            if([[XTProtocol instance]isConnected])
                [[XTProtocol instance]writeMLXRegister:TS_EEPROM_EMMISIVITY withValue:round(65535.0 * value.floatValue)];
        }
        self.emissivityText.text = @"";
        [self.emissivityText resignFirstResponder];
    }
    else if(settlingTimeText == textField)
    {
        DDLogVerbose(@"You entered %@",self.settlingTimeText.text);
        NSNumber* value = [f numberFromString:self.settlingTimeText.text];
        [XTProtocol instance].settlingTime = value.intValue;
        [self.settlingTimeText resignFirstResponder];
    }
    else if(samplingIntervalText == textField)
    {
        DDLogVerbose(@"You entered %@",self.samplingIntervalText.text);
        NSNumber* value = [f numberFromString:self.samplingIntervalText.text];
        [XTProtocol instance].samplingInterval = value.intValue;
        [self.samplingIntervalText resignFirstResponder];
    }
    else if(firText == textField)
    {
        DDLogVerbose(@"You entered %@",self.firText.text);
        NSNumber* value = [f numberFromString:self.firText.text];
        if( (value.intValue < 0) || (value.intValue > 7) )
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"FIR value is between 0 and 7, please try again." delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        else{
            // Valid Ke, update it
            if([[XTProtocol instance]isConnected])
                [[XTProtocol instance]writeConfigRegister:(uint8_t)value.intValue withParam:[XTProtocol instance].configIIR];
        }
        self.firText.text = @"";
        [self.firText resignFirstResponder];
    }
    else if(iirText == textField)
    {
        DDLogVerbose(@"You entered %@",self.iirText.text);
        NSNumber* value = [f numberFromString:self.iirText.text];
        if( (value.intValue < 0) || (value.intValue > 7) )
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"IIR value is between 0 and 7, please try again." delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        else{
            // Valid Ke, update it
            if([[XTProtocol instance]isConnected])
                [[XTProtocol instance]writeConfigRegister:[XTProtocol instance].configFIR withParam:(uint8_t)value.intValue ];
        }
        self.iirText.text = @"";
        [self.iirText resignFirstResponder];
    }
    else if(offsetText == textField)
    {
        DDLogVerbose(@"You entered %@",self.offsetText.text);
        NSNumber* value = [f numberFromString:self.offsetText.text];
        if( (value.floatValue < 0.0) || (value.floatValue > 50.0) )
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Offset value is between 0 and 50, please try again." delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        else{
            [XTProtocol instance].offsetBodyMode = value.floatValue;
        }
        [self.offsetText resignFirstResponder];
    }
    
    return YES;
    
}

- (void) accessoryParametersChanged:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"XTParametersChangedNotification"]){
        [self updateParameters];
    }
    
}

- (void) updateParameters
{
    if ([NSThread isMainThread]) {
        // do the UI stuff as normal
        self.emissivityText.text = [[[XTProtocol instance]getEmissivity]stringValue];
        self.firText.text = [NSString stringWithFormat:@"%d",[XTProtocol instance].configFIR] ;
        self.iirText.text = [NSString stringWithFormat:@"%d",[XTProtocol instance].configIIR] ;
    } else {
        [self performSelectorOnMainThread:@selector(updateParameters) withObject:nil waitUntilDone:NO];
    }
    
}

@end

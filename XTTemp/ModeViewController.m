//
//  ModeViewController.m
//  SmartpacT
//
//  Created by KG on 7/1/14.
//  Copyright (c) 2014 Xenon Technologies Pte Ltd. All rights reserved.
//

#import "ModeViewController.h"

@interface ModeViewController ()

@end

@implementation ModeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void) segmentControlChanged:(id)sender{
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    [XTProtocol instance].bodyMode = (bool)[segmentedControl selectedSegmentIndex];
    // offset will be updated whenever there is a change from the settings VC

}

@end

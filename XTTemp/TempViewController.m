//
//  FirstViewController.m
//  XTTemp
//
//  Created by KG on 21/10/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import "TempViewController.h"
#import "XTProtocol.h"

@interface TempViewController ()

@end

@implementation TempViewController

#if (LOGGING == 1)
@synthesize fileLogger;
#endif

@synthesize tempTableView;
@synthesize labelAccStatus;
@synthesize labelMode;
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#define C_DEFAULT_SETTLING_TIME_MS 500
#define C_DEFAULT_SAMPLING_INTERVAL_MS 500
#define C_DEFAULT_AVG_POINTS 5

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //  TemperatureArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:0.0f],nil];
#if (LOGGING == 1)
    // Create Log File
    fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 0; //60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 0;//7;
    // Govind - Enable Logging
    [DDLog addLogger:fileLogger];
#endif
    
    [XTProtocol instance].delegate = self; // subscribe to accessory conn/disconn status
    [XTProtocol instance].delegateXT = self; // subscribe to temp events
    firstConnect = TRUE;
    // Audio Player Reference
    NSString *soundPath = [[NSBundle mainBundle]pathForResource:@"beep" ofType:@"wav"];
    NSURL *soundURL = [[NSURL alloc]initFileURLWithPath:soundPath];
    _audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:soundURL error:nil];
    // Formatter for display of numbers: http://useyourloaf.com/blog/2012/06/14/using-number-formatters.html
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    [self.numberFormatter setMaximumFractionDigits:1];
    [self.numberFormatter setRoundingMode:NSNumberFormatterRoundDown];
    self.objectTemperature.text = @"";
    [self updateConnectionStatus]; // Start the timer if accessory is connected
    [self loadDefaults];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(StopStart:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(StopStart:)
                                                 name: UIApplicationWillEnterForegroundNotification
                                               object: nil];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)StopStart:(NSNotification *)notification
{
    if(notification.name == UIApplicationDidEnterBackgroundNotification){
        // Clear everything including mesaurements in progress, before we enter into background
        // Stop refresh timer
        if (_samplingTimer != nil)
        {
            [_samplingTimer invalidate];
            _samplingTimer = nil;
        }
        measurementInProgress = FALSE;
        [[XTProtocol instance] updateAccStatus:false];
    }
    else if(notification.name == UIApplicationWillEnterForegroundNotification){
        if([[XTProtocol instance] isConnected])
            [[XTProtocol instance] resetXTState ];
        [self UpdateConnectionStatusText];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if([XTProtocol instance].bodyMode == true)
        labelMode.text = @"Body";
    else
        labelMode.text = @"Object";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.debugMode == FALSE){
        [self fillDemoScreen];
    }
    else{
        [self fillDebugScreen];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    

}

- (void)loadDefaults
{
    self.displayMode = C_MODE_DEMO;
    [XTProtocol instance].settlingTime = C_DEFAULT_SETTLING_TIME_MS;
    [XTProtocol instance].samplingInterval = C_DEFAULT_SAMPLING_INTERVAL_MS;
    [[XTProtocol instance]setAvgPoints:[[NSNumber alloc]initWithInt:C_DEFAULT_AVG_POINTS ]];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) resetState{
    /*
     DDLogVerbose(@"--- Reset State---");
     [self clearInternalFlags];
     if([demo isConnected]){
     [demo clearFlashStatus];
     [demo clearTrigger];
     [demo newFlashStatusCmd];
     }
     */
    
}

- (void) readTemperatureCallback
{
    
    if([[XTProtocol instance] isConnected])
        [[XTProtocol instance] readTemperature];
    DDLogVerbose(@"read Temperature");
    // [self UpdateTemperatureView];
    
    
}





- (void) batteryPackAlert:(uint8_t)withType
{
    switch (withType) {
        case C_BAT_PACK_VBAT_LOW:
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Battery Pack Status" message:@"Battery Pack does not have enough power to charge Xenon Flash, please recharge battery pack." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
            break;
            
        case C_BAT_PACK_FLT:
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Battery Pack Status" message:@"Battery Pack Fault Detected, Please recharge and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark - Measurement Control
// Must be executed on main thread
- (bool) startTempMeasurement
{
        DDLogVerbose(@"--- Measurement Started ---");
    bool ret = FALSE;
        if(([[XTProtocol instance] isConnected] == false) || (measurementInProgress == TRUE) )
            return(ret);
        
        measurementInProgress = TRUE;
        // Stop refresh timer
        if (_samplingTimer != nil)
        {
            [_samplingTimer invalidate];
            _samplingTimer = nil;
        }
        // Clear internal buffer
        [[XTProtocol instance]startNewMeasurement];
        
        [self performSelector:@selector(startTempReadbackTimer) withObject:self afterDelay:((float)[XTProtocol instance].settlingTime/1000)];
        
        labelAccStatus.text = @"Measuring";
        DDLogVerbose(@"Measuring, Settling time = %d, Sampling Time = %d, %f, %f",[XTProtocol instance].settlingTime,[XTProtocol instance].samplingInterval,((float)[XTProtocol instance].settlingTime/1000),((float)[XTProtocol instance].samplingInterval/1000) );
        

    return(ret);
}

- (void) startTempReadbackTimer
{
    [self readTemperatureCallback]; // trigger the first read
    // Restart timer
    _samplingTimer = [NSTimer scheduledTimerWithTimeInterval:((float)[XTProtocol instance].samplingInterval/1000)  target:self selector:@selector(readTemperatureCallback) userInfo:nil repeats:YES];
    
}

- (void) newMeasurementComplete:(float)temp
{
    // Timer should only be stopped in the thread that created it
    dispatch_async(dispatch_get_main_queue(), ^{
        
        DDLogVerbose(@"--- Measurement Complete ---");
        DDLogVerbose(@"Result: T_AMBIENT = %3.3f, T_OBJECT = %3.3f", [[[[XTProtocol instance] TemperatureData]objectAtIndex:C_INDEX_AMBIENT_TEMP]floatValue],
                     [[[[XTProtocol instance] TemperatureData]objectAtIndex:C_INDEX_OBJECT_TEMP]floatValue]);
        // Stop refresh timer
        if (_samplingTimer != nil)
        {
            [_samplingTimer invalidate];
            _samplingTimer = nil;
        }
        // make a buzzer sound
        [self playBeep];
        // Display the last 10 temperature values as a list
        [self UpdateTemperatureView];
        [self UpdateConnectionStatusText];
#if(LOGGING==1)
        [[self fileLogger ]rollLogFile];
#endif
        measurementInProgress = FALSE;
        if([[XTProtocol instance] isConnected])
            [[XTProtocol instance] resetXTState ];
    });
    
}

- (void) shutterPressed
{
    DDLogVerbose(@"--- Shutter Pressed ---");
    
    [self performSelectorOnMainThread:@selector(startTempMeasurement) withObject:nil waitUntilDone:NO];
    
    
}
- (void) playBeep
{
    if ([NSThread isMainThread]) {
        // do the UI stuff as normal
        [_audioPlayer play];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    } else {
        [self performSelectorOnMainThread:@selector(playBeep) withObject:nil waitUntilDone:NO];
    }
    
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString* header = @"";
    switch(section)
    {
        case 0:
            header = @"Ambient Temperature";
            break;
        case 1:
            header = @"Object Temperature";
            break;
            
        case 2:
            header = @"Raw IR Value 1";
            break;
            
        case 3:
            header = @"Raw IR Value 2";
            break;
            
            
    }
    
    
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    // Usually the number of items in your array (the one that holds your list)
    return C_NO_OF_ROWS_PER_SECTION;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Where we configure the cell in each row
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // the row index is local to the section
    // http://stackoverflow.com/questions/2107603/correct-indexpath-row-selection-in-tableview-which-has-multiple-sections
    NSUInteger row = indexPath.row;
    NSUInteger sect = indexPath.section;
    // Configure the cell... setting the text of our cell's label
    cell.textLabel.text = [self.numberFormatter stringFromNumber:[[[XTProtocol instance] TemperatureData] objectAtIndex:row * C_NO_OF_ROWS_PER_SECTION + sect]] ;//[items objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - Connect/Disconnect

- (void) UpdateConnectionStatusText
{
    if ([NSThread isMainThread]) {
        if([[XTProtocol instance] isConnected] == true)
            labelAccStatus.text = @"Ready";
        else
            labelAccStatus.text = @"Disconnected";
    }
    else{
        [self performSelectorOnMainThread:@selector(UpdateConnectionStatusText) withObject:nil waitUntilDone:NO];
    }
}

- (void) updateConnectionStatus
{
    if([[XTProtocol instance] isConnected])
    {
        if(firstConnect) // Re-init the accessory on a first connect
            [self resetState];
        
        firstConnect = FALSE;
        DDLogVerbose(@"--- Connected----");
    }
    else{
        firstConnect = TRUE;
        DDLogVerbose(@"--- DIS-Connected----");
        [[XTProtocol instance] updateAccStatus:false];
        if (_samplingTimer != nil)
        {
            [_samplingTimer invalidate];
            _samplingTimer = nil;
        }
    }
    [self UpdateConnectionStatusText];
    
}

- (void) accessoryConnectionStatusChanged:(bool) status
{
    if ([NSThread isMainThread]) {
        // do the UI stuff as normal
        [self updateConnectionStatus];
    } else {
        //dispatch_async(dispatch_get_main_queue(), ^{ UI stuff });
        [self performSelectorOnMainThread:@selector(updateConnectionStatus) withObject:nil waitUntilDone:NO];
    }
}

#pragma mark - UI Stuff

- (void) UpdateTemperatureView
{
    if ([NSThread isMainThread]) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        // do the UI stuff as normal
        [[self tempTableView]reloadData]; // refresh the table
        if(appDelegate.debugMode == FALSE){
#if 0
            NSString* disp ;
            disp = [self.numberFormatter stringFromNumber:[[[XTProtocol instance] TemperatureData] objectAtIndex:C_INDEX_OBJECT_TEMP]] ;
            disp = [disp stringByAppendingString:@"ºC"];
            self.objectTemperature.text = disp;
#endif
            [self fillDemoScreen];
        }
        else{
            [self fillDebugScreen];
        }
        
    } else {
        [self performSelectorOnMainThread:@selector(UpdateTemperatureView) withObject:nil waitUntilDone:NO];
    }
    
}

#define C_DISP_START_X  20
#define C_DISP_START_X2  200
#define C_DISP_START_Y  30 //spacing
#define C_DISP_WIDTH    150
#define C_DISP_HEIGHT    20

#define C_CONN_DISP_X   160
#define C_CONN_DISP_Y   10
#define C_CONN_DISP_WIDTH 150
#define C_CONN_DISP_HEIGHT 20

#define C_TEMP_DISP_X   80
#define C_TEMP_DISP_Y   200
#define C_TEMP_DISP_WIDTH 180
#define C_TEMP_DISP_HEIGHT 75

#define C_MODE_DISP_X   130
#define C_MODE_DISP_Y   490
#define C_MODE_DISP_WIDTH 200
#define C_MODE_DISP_HEIGHT 25

- (void) fillDemoScreen
{
    // Clear screen
    for(UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
    NSString* disp ;
    disp = [self.numberFormatter stringFromNumber:[[[XTProtocol instance] TemperatureData] objectAtIndex:C_INDEX_OBJECT_TEMP]] ;
    disp = [disp stringByAppendingString:@"ºC"];
    
    // Create the temp label and Connected status
    UILabel* temp =  [[UILabel alloc] initWithFrame:CGRectMake(C_TEMP_DISP_X, C_TEMP_DISP_Y, C_TEMP_DISP_WIDTH, C_TEMP_DISP_HEIGHT)];
    [temp  setText:disp];
    [temp setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:50]];
    [temp setTextColor:[UIColor whiteColor]];
    temp.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:temp];
    self.objectTemperature = temp;
    
    UILabel* conn =  [[UILabel alloc] initWithFrame:CGRectMake(C_CONN_DISP_X, C_CONN_DISP_Y, C_CONN_DISP_WIDTH, C_CONN_DISP_HEIGHT)];
    [conn  setText:disp];
    [conn setFont:[UIFont fontWithName:@"Courier" size:20]];
    [conn setTextColor:[UIColor whiteColor]];
    conn.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:conn];
    self.labelAccStatus = conn;
    [self UpdateConnectionStatusText];
    
    UILabel* mode =  [[UILabel alloc] initWithFrame:CGRectMake(C_MODE_DISP_X, C_MODE_DISP_Y, C_MODE_DISP_WIDTH, C_MODE_DISP_HEIGHT)];
    [mode  setText:disp];
    [mode setFont:[UIFont fontWithName:@"Courier" size:20]];
    [mode setTextColor:[UIColor whiteColor]];
    [self.view addSubview:mode];
    self.labelMode = mode;
    if([XTProtocol instance].bodyMode == true)
        labelMode.text = @"Body";
    else
        labelMode.text = @"Object";
}

- (void) fillDebugScreen
{
    int j = 0;
    // Array of UIlabels to store all the information
    NSMutableArray* AmbLabel = [[NSMutableArray alloc]init];
    for(UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
    
    UIColor* default_color = [UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50];
    // Heading
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X, C_DISP_START_Y, C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"%@", @"Amb Temp"]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:default_color];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    
    // Called when information is ready for display
    for(int i=0; i<[XTProtocol instance].AmbTempBuffer.count; i++, j++){
        [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
        [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"%2.2f", [[[XTProtocol instance].AmbTempBuffer objectAtIndex:i]floatValue]]];
        [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
        [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
        [self.view addSubview:[AmbLabel objectAtIndex:j]];
    }
    // Min
    double min = [[[XTProtocol instance].AmbTempBuffer objectAtIndex:0]floatValue];
    for(int i=0; i<[XTProtocol instance].AmbTempBuffer.count; i++){
        if(min > [[[XTProtocol instance].AmbTempBuffer objectAtIndex:i]floatValue])
            min = [[[XTProtocol instance].AmbTempBuffer objectAtIndex:i]floatValue];
    }
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Min:%2.2f", min]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    // Max
    double max = [[[XTProtocol instance].AmbTempBuffer objectAtIndex:0]floatValue];
    for(int i=0; i<[XTProtocol instance].AmbTempBuffer.count; i++){
        if(max < [[[XTProtocol instance].AmbTempBuffer objectAtIndex:i]floatValue])
            max = [[[XTProtocol instance].AmbTempBuffer objectAtIndex:i]floatValue];
    }
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Max:%2.2f", max]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    // Mean
    double mean =0 ;
    for(int i=0; i<[XTProtocol instance].AmbTempBuffer.count; i++){
        mean += [[[XTProtocol instance].AmbTempBuffer objectAtIndex:0]floatValue];
    }
    mean = mean / [XTProtocol instance].AmbTempBuffer.count;
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Mean:%2.2f", mean]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    
    
    [AmbLabel removeAllObjects];
    // Heading
    j = 0;
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X2, C_DISP_START_Y, C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"%@", @"Obj Temp"]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    
    for(int i=0; i<[XTProtocol instance].ObjTempBuffer.count; i++, j++){
        [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X2, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
        [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"%2.2f", [[[XTProtocol instance].ObjTempBuffer objectAtIndex:i]floatValue]]];
        [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
        [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
        [self.view addSubview:[AmbLabel objectAtIndex:j]];
    }
    
    // Min
    min = [[[XTProtocol instance].ObjTempBuffer objectAtIndex:0]floatValue];
    for(int i=0; i<[XTProtocol instance].ObjTempBuffer.count; i++){
        if(min > [[[XTProtocol instance].ObjTempBuffer objectAtIndex:i]floatValue])
            min = [[[XTProtocol instance].ObjTempBuffer objectAtIndex:i]floatValue];
    }
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X2, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Min:%2.2f", min]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    // Max
    max = [[[XTProtocol instance].ObjTempBuffer objectAtIndex:0]floatValue];
    for(int i=0; i<[XTProtocol instance].ObjTempBuffer.count; i++){
        if(max < [[[XTProtocol instance].ObjTempBuffer objectAtIndex:i]floatValue])
            max = [[[XTProtocol instance].ObjTempBuffer objectAtIndex:i]floatValue];
    }
    
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X2, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Max:%2.2f", max]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    j++;
    // Mean
    mean =0 ;
    for(int i=0; i<[XTProtocol instance].ObjTempBuffer.count; i++){
        mean += [[[XTProtocol instance].ObjTempBuffer objectAtIndex:0]floatValue];
    }
    mean = mean / [XTProtocol instance].ObjTempBuffer.count;
    [AmbLabel addObject: [[UILabel alloc] initWithFrame:CGRectMake(C_DISP_START_X2, C_DISP_START_Y * (j+1), C_DISP_WIDTH, C_DISP_HEIGHT)]];
    [(UILabel*)[AmbLabel objectAtIndex:j] setText:[NSString stringWithFormat:@"Mean:%2.2f", mean]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setFont:[UIFont fontWithName:@"Courier" size:20]];
    [(UILabel*)[AmbLabel objectAtIndex:j]setTextColor:[UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.50]];
    [self.view addSubview:[AmbLabel objectAtIndex:j]];
    
    // Connection status
    UILabel* conn =  [[UILabel alloc] initWithFrame:CGRectMake(C_CONN_DISP_X, C_CONN_DISP_Y, C_CONN_DISP_WIDTH, C_TEMP_DISP_HEIGHT)];
    [conn setFont:[UIFont fontWithName:@"Courier" size:20]];
    [conn setTextColor:[UIColor whiteColor]];
    [self.view addSubview:conn];
    self.labelAccStatus = conn;
    [self UpdateConnectionStatusText];
}

@end


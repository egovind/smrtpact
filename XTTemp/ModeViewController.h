//
//  ModeViewController.h
//  SmartpacT
//
//  Created by KG on 7/1/14.
//  Copyright (c) 2014 Xenon Technologies Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XTProtocol.h"

@interface ModeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *operationMode;
-(IBAction)segmentControlChanged:(id)sender;

@end

//
//  SecondViewController.h
//  XTTemp
//
//  Created by KG on 21/10/13.
//  Copyright (c) 2013 Xenon Technologies Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"
#import "XTProtocol.h"
#import "AppDelegate.h"



@interface SettingsViewController :  UIViewController <UITextFieldDelegate>
{

}


@property (strong, nonatomic) IBOutlet UITextField *emissivityText;
@property (strong, nonatomic) IBOutlet UITextField *avgPointsText;
@property (strong, nonatomic) IBOutlet UITextField *settlingTimeText;
@property (strong, nonatomic) IBOutlet UITextField *samplingIntervalText;
@property (strong, nonatomic) IBOutlet UITextField *firText;
@property (strong, nonatomic) IBOutlet UITextField *iirText;
@property (strong, nonatomic) IBOutlet UITextField *offsetText;


- (BOOL)textFieldShouldReturn:(UITextField *)textField;
-(IBAction)kEEditingStarted:(id)sender;
-(IBAction)kEEditingFinished:(id)sender;
-(IBAction)enterDebugMode:(id)sender;
-(IBAction)enterDemoMode:(id)sender;


@end
